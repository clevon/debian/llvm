Fixes a potential std::atomic<T*>::wait() hang.
Implementation based on analysis and prior work in https://reviews.llvm.org/D114119.

# Waiting ordering
The sequential consistency order in __libcpp_contention_wait() on the waiters increment doesn't imply happens-before on the waiters check in __libcpp_contention_notify().
Using a "read-don't-modify-write" operation in __libcpp_contention_notify() makes this ordering impossible.
This is quite mind-bending.

# Load-value timing
__backoff_fn() and __test_fn() doing own loads of the value, which might differ.
The value loaded should be passed into the backoff function into the test function.
However this is an issue for std::atomic<T*> as the monitor value is 32-bit while T* is 64-bit.
I'm unsure how to fix this correctly so I've omitted the fix for this.
--- a/libcxx/src/atomic.cpp
+++ b/libcxx/src/atomic.cpp
@@ -138,7 +138,7 @@ static __libcpp_contention_table_entry*
 static void __libcpp_contention_notify(__cxx_atomic_contention_t volatile* __contention_state,
                                        __cxx_atomic_contention_t const volatile* __platform_state,
                                        bool __notify_one) {
-  if (0 != __cxx_atomic_load(__contention_state, memory_order_seq_cst))
+  if (0 != __cxx_atomic_fetch_add(__contention_state, 0, memory_order_release))
     // We only call 'wake' if we consumed a contention bit here.
     __libcpp_platform_wake_by_address(__platform_state, __notify_one);
 }
@@ -151,7 +151,7 @@ __libcpp_contention_monitor_for_wait(__c
 static void __libcpp_contention_wait(__cxx_atomic_contention_t volatile* __contention_state,
                                      __cxx_atomic_contention_t const volatile* __platform_state,
                                      __cxx_contention_t __old_value) {
-  __cxx_atomic_fetch_add(__contention_state, __cxx_contention_t(1), memory_order_seq_cst);
+  __cxx_atomic_fetch_add(__contention_state, __cxx_contention_t(1), memory_order_acquire);
   // We sleep as long as the monitored value hasn't changed.
   __libcpp_platform_wait_on_address(__platform_state, __old_value);
   __cxx_atomic_fetch_sub(__contention_state, __cxx_contention_t(1), memory_order_release);
