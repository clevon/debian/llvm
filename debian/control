Source: llvm
Section: devel
Priority: optional
Maintainer: Mairi Dubik <mairi.dubik@clevon.com>
Homepage: https://www.llvm.org/
Vcs-Browser: https://gitlab.com/clevon/debian/llvm
Vcs-Git: https://gitlab.com/clevon/debian/llvm.git
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends:
 binutils-dev [!arm64],
 cmake,
 debhelper-compat (= 13),
 dh-exec,
 doxygen <!nodoc>,
 furo <!nodoc>,
 graphviz,
 libatomic1,
 libc++-dev,
 libclangrt,
 libedit-dev,
 libjs-mathjax,
 libjsoncpp-dev,
 libncurses5-dev,
 libpfm4-dev,
 libpython3-dev,
 libz3-dev,
 llvm:native,
 ninja-build,
 picolibc-arm-none-eabi,
 pkg-config,
 python3-myst-parser <!nodoc>,
 python3-psutil <!nocheck>,
 python3-recommonmark <!nodoc>,
 python3-setuptools,
 python3-sphinx-automodapi <!nodoc>,
 python3:native,
 swig,
 texinfo <!nodoc>,
 zlib1g-dev,

Package: bolt
Architecture: amd64 arm64
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Post-link optimizer
 Optimizes binaries' code layout after linking based on profile data to improve
 performance of large applications.

Package: build-essential-clang
Architecture: any
Multi-Arch: foreign
Depends: clang, dpkg-dev, libc-dev, make, ${misc:Depends}
Provides: build-essential
Description: LLVM-based build-essential metapackage
 This provides an LLVM-based alternative to the GNU GCC based build-essential
 package.

Package: clang
Architecture: any
Depends:
 binutils,
 libc++-dev,
 libc++abi-dev,
 libc-dev,
 libclang-common-dev (= ${source:Version}),
 libclang1 (= ${binary:Version}),
 libclangrt,
 ${misc:Depends},
 ${shlibs:Depends},
Provides: c++-compiler, c-compiler, objc-compiler
Recommends: llvm-dev, python3
Suggests: clang-doc
Description: C, C++, CUDA and Objective C/C++ compiler
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.

Package: clang-doc
Section: doc
Architecture: all
Depends: libjs-mathjax, ${misc:Depends}
Description: C, C++, CUDA and Objective C/C++ compiler (documentation)
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.

Package: clang-format
Architecture: any
Depends:
 libllvm (= ${binary:Version}),
 python3,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Tool to format C/C++/Obj-C code
 Clang-format is both a library and a stand-alone tool with the goal of
 automatically reformatting C++ sources files according to configurable style
 guides. To do so, clang-format uses Clang's Lexer to transform an input file
 into a token stream and then changes all the whitespace around those tokens.
 The goal is for clang-format to both serve both as a user tool (ideally with
 powerful IDE integrations) and part of other refactoring tools, e.g. to do a
 reformatting of all the lines changed during a renaming.
 .
 This package also provides vim and emacs plugins.

Package: clang-tidy
Architecture: any
Depends:
 clang-tools,
 libclang-common-dev,
 libllvm (= ${binary:Version}),
 python3,
 python3-yaml,
 ${misc:Depends},
 ${shlibs:Depends},
Description: clang-based C++ linter tool
 Provide an extensible framework for diagnosing and fixing typical programming
 errors, like style violations, interface misuse, or bugs that can be deduced
 via static analysis. clang-tidy is modular and provides a convenient interface
 for writing new checks.

Package: clang-tools
Architecture: any
Suggests: clang-tools-doc
Depends:
 clang (= ${binary:Version}),
 python3,
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Description: clang-based tools for C/C++ development
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This package contains some clang-based tools like scan-build, clang-cl, etc.

Package: clang-tools-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: clang-based tools for C/C++ development (documentation)
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.

Package: clangd
Architecture: any
Depends:
 libclang-common-dev (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Language server that provides IDE-like features to editors
 clangd understands your C++ code and adds smart features to your editor:
  - code completion
  - compile errors
  - go-to-definition
  - and more.
 .
 clangd is a language server that implements the Language Server Protocol;
 it can work with many editors through a plugin.

Package: libbolt-dev
Section: libdevel
Architecture: amd64 arm64
Depends: ${misc:Depends}
Description: Post-link optimizer (static runtime libraries)
 Optimizes binaries' code layout after linking based on profile data to improve
 performance of large applications.

Package: libc++-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libc++2 (= ${binary:Version}), ${misc:Depends}
Provides: libc++-x.y-dev
Conflicts: libc++abi-17-dev (= 1:17.0.6-1), libc++-x.y-dev
Replaces: libc++-x.y-dev
Description: LLVM C++ Standard library (development files)
 libc++ is another implementation of the C++ standard library
 .
 Features and Goals
 .
   * Correctness as defined by the C++ standards.
   * Fast execution.
   * Minimal memory use.
   * Fast compile times.
   * ABI compatibility with gcc's libstdc++ for some low-level features such
     as exception objects, rtti and memory allocation.
   * Extensive unit tests.

Package: libc++2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Suggests: clang
Description: LLVM C++ Standard library
 libc++ is another implementation of the C++ standard library.
 .
 Features and Goals
 .
   * Correctness as defined by the C++ standards.
   * Fast execution.
   * Minimal memory use.
   * Fast compile times.
   * ABI compatibility with gcc's libstdc++ for some low-level features such
     as exception objects, rtti and memory allocation.
   * Extensive unit tests.

Package: libc++abi-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libc++abi1 (= ${binary:Version}), ${misc:Depends}
Provides: libc++abi-x.y-dev
Conflicts: libc++abi-x.y-dev
Replaces: libc++abi-x.y-dev
Description: LLVM low-level support for a standard C++ library (development files)
 libc++abi is another implementation of low-level support for a standard C++
 library.
 .
 Features and Goals
 .
   * Correctness as defined by the C++ standards.
   * Provide a portable sublayer to ease the porting of libc++

Package: libc++abi1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Provides: libc++abi-x.y
Conflicts: libc++abi-x.y
Replaces: libc++abi-x.y
Description: LLVM low-level support for a standard C++ library
 libc++abi is another implementation of low-level support for a standard C++
 library.
 .
 Features and Goals
 .
   * Correctness as defined by the C++ standards.
   * Provide a portable sublayer to ease the porting of libc++

Package: libclang-common-dev
Section: libdevel
Architecture: all
Depends: libllvm (>= ${source:Version}), ${misc:Depends}
Description: Clang library (common development files)
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This package contains the Clang generic headers.

Package: libclang-cpp
Section: libs
Architecture: any
Multi-Arch: same
Depends: libllvm (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: C++ interface to the Clang library
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This package contains the Clang C++ library.
 .
 The C++ Interface to Clang provides an API that exposes facilities for parsing
 source code into an abstract syntax tree (AST), loading already-parsed ASTs,
 traversing the AST, associating physical source locations with elements within
 the AST, and other facilities that support Clang-based development tools.

Package: libclang-cpp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libclang-cpp (= ${binary:Version}), ${misc:Depends}
Description: C++ interface to the Clang library
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This package contains the Clang C++ library.
 .
 The C++ Interface to Clang provides an API that exposes facilities for parsing
 source code into an abstract syntax tree (AST), loading already-parsed ASTs,
 traversing the AST, associating physical source locations with elements within
 the AST, and other facilities that support Clang-based development tools.
 .
 This package contains the Clang headers to develop extensions over
 libclang-cpp.

Package: libclang-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libclang-common-dev (= ${source:Version}),
 libclang1 (= ${binary:Version}),
 ${misc:Depends},
Description: Clang library (development files)
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This package contains the Clang headers to develop extensions over libclang1.

Package: libclang1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: C interface to the Clang library
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This package contains the Clang library.
 .
 The C Interface to Clang provides a relatively small API that exposes
 facilities for parsing source code into an abstract syntax tree (AST),
 loading already-parsed ASTs, traversing the AST, associating physical source
 locations with elements within the AST, and other facilities that support
 Clang-based development tools.

Package: libclangrt
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: libclangrt-${Arch}
Breaks: libclangrt-${Arch}
Description: Clang runtime libraries (${Arch})
 Compiler support routines from the compiler-rt project for use by Clang.

Package: libclangrt-armv7em
Section: libdevel
Architecture: all
Depends: ${misc:Depends}
Description: Clang runtime libraries (armv7em)
 Compiler support routines from the compiler-rt project for use by Clang.
 .
 This package contains the runtimes for armv7em.

Package: liblld
Section: libs
Architecture: any
Multi-Arch: same
Depends: libllvm (= ${binary:Version}), ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: LLVM-based linker, library
 LLD is a new, high-performance linker. It is built as a set of reusable
 components which highly leverage existing libraries in the larger LLVM
 Project.
 .
 This package contains the LLD runtime library.

Package: liblld-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblld (= ${binary:Version}),
 lld (= ${binary:Version}),
 ${misc:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: LLVM-based linker (development files)
 LLD is a new, high-performance linker. It is built as a set of reusable
 components which highly leverage existing libraries in the larger LLVM
 Project.
 .
 This package provides the header files to build extensions over lld.

Package: liblldb
Section: libs
Architecture: any
Multi-Arch: same
Depends: libllvm (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Next generation, high-performance debugger
 LLDB is a next generation, high-performance debugger. It is built as a set of
 reusable components which highly leverage existing libraries in the larger
 LLVM project, such as the Clang expression parser and LLVM disassembler.
 .
 This package contains the LLDB runtime library.

Package: liblldb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: lldb (= ${binary:Version}), ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Next generation, high-performance debugger (development files)
 LLDB is a next generation, high-performance debugger. It is built as a set of
 reusable components which highly leverage existing libraries in the larger
 LLVM project, such as the Clang expression parser and LLVM disassembler.
 .
 This package provides the header files to build extensions over lldb.

Package: libllvm
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Modular compiler and toolchain technologies
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 This package contains the LLVM runtime library.

Package: libpolly
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: LLVM polyhedral optimization infrastructure
 Polly is a high-level loop and data-locality optimizer and optimization
 infrastructure for LLVM. It uses an abstract mathematical representation based
 on integer polyhedra to analyze and optimize the memory access pattern of a
 program.

Package: libpolly-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Suggests: libpolly-doc
Depends: libpolly (= ${binary:Version}), ${misc:Depends}
Provides: libpolly-x.y-dev
Conflicts: libpolly-x.y-dev
Replaces: libpolly-x.y-dev
Description: LLVM polyhedral optimization infrastructure (development files)
 Polly is a high-level loop and data-locality optimizer and optimization
 infrastructure for LLVM. It uses an abstract mathematical representation based
 on integer polyhedra to analyze and optimize the memory access pattern of a
 program.

Package: libpolly-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: LLVM polyhedral optimization infrastructure (documentation)
 Polly is a high-level loop and data-locality optimizer and optimization
 infrastructure for LLVM. It uses an abstract mathematical representation based
 on integer polyhedra to analyze and optimize the memory access pattern of a
 program.

Package: libunwind1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Provides: libunwind-x.y
Conflicts: libunwind-x.y
Replaces: libunwind-x.y
Description: LLVM implementation of the libunwind interface
 libunwind is intended to be a small and fast implementation of the ABI,
 leaving off some features of HP's libunwind that never materialized (e.g.
 remote unwinding).

Package: libunwind1-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libunwind1 (= ${binary:Version}), ${misc:Depends}
Provides: libunwind-x.y-dev
Conflicts: libunwind-x.y-dev
Replaces: libunwind-x.y-dev
Description: LLVM implementation of the libunwind interface (development files)
 libunwind is intended to be a small and fast implementation of the ABI,
 leaving off some features of HP's libunwind that never materialized (e.g.
 remote unwinding).

Package: lld
Architecture: any
Suggests: lld-doc
Depends: libllvm (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: LLVM-based linker
 LLD is a new, high-performance linker. It is built as a set of reusable
 components which highly leverage existing libraries in the larger LLVM
 Project.

Package: lld-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: LLVM-based linker (documentation)
 LLD is a new, high-performance linker. It is built as a set of reusable
 components which highly leverage existing libraries in the larger LLVM
 Project.

Package: lldb
Architecture: any
Suggests: lldb-doc
Depends:
 libclang-cpp (= ${binary:Version}),
 libllvm (= ${binary:Version}),
 python3-lldb,
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends: ${misc:Pre-Depends}
Description: Next generation, high-performance debugger
 LLDB is a next generation, high-performance debugger. It is built as a set of
 reusable components which highly leverage existing libraries in the larger
 LLVM project, such as the Clang expression parser and LLVM disassembler.

Package: lldb-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: Next generation, high-performance debugger (documentation)
 LLDB is a next generation, high-performance debugger. It is built as a set of
 reusable components which highly leverage existing libraries in the larger
 LLVM project, such as the Clang expression parser and LLVM disassembler.

Package: llvm
Architecture: any
Suggests: llvm-doc
Depends: llvm-runtime (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Recommends: llvm-dev
Description: Modular compiler and toolchain technologies
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 LLVM uses a single, language-independent virtual instruction set both as an
 offline code representation (to communicate code between compiler phases and
 to run-time systems) and as the compiler internal representation (to analyze
 and transform programs). This persistent code representation allows a common
 set of sophisticated compiler techniques to be applied at compile-time,
 link-time, install-time, run-time, or "idle-time" (between program runs).
 .
 The strengths of the LLVM infrastructure are its source-language independence,
 powerful mid-level optimizer, automated compiler debugging support,
 extensibility, stability and reliability. LLVM includes C, C++, CUDA, OpenMP
 and Fortran front-ends. LLVM can generate code for all major and some lesser
 architectures of the day.

Package: llvm-dev
Architecture: any
Depends:
 libclang-cpp (= ${binary:Version}),
 libffi-dev,
 libllvm (= ${binary:Version}),
 libtinfo-dev,
 libz3-dev,
 llvm (= ${binary:Version}),
 llvm-tools (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Modular compiler and toolchain technologies (development files)
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 LLVM uses a single, language-independent virtual instruction set both as an
 offline code representation (to communicate code between compiler phases and
 to run-time systems) and as the compiler internal representation (to analyze
 and transform programs). This persistent code representation allows a common
 set of sophisticated compiler techniques to be applied at compile-time,
 link-time, install-time, run-time, or "idle-time" (between program runs).
 .
 This package provides the libraries and headers to develop applications using
 llvm.

Package: llvm-doc
Section: doc
Architecture: all
Depends: libjs-jquery, libjs-underscore, ${misc:Depends}
Description: Modular compiler and toolchain technologies (documentation)
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 LLVM uses a single, language-independent virtual instruction set both as an
 offline code representation (to communicate code between compiler phases and
 to run-time systems) and as the compiler internal representation (to analyze
 and transform programs). This persistent code representation allows a common
 set of sophisticated compiler techniques to be applied at compile-time,
 link-time, install-time, run-time, or "idle-time" (between program runs).

Package: llvm-examples
Section: doc
Architecture: all
Depends: llvm-dev (>= ${source:Version}), ${misc:Depends}
Description: Modular compiler and toolchain technologies (examples)
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 LLVM uses a single, language-independent virtual instruction set both as an
 offline code representation (to communicate code between compiler phases and
 to run-time systems) and as the compiler internal representation (to analyze
 and transform programs). This persistent code representation allows a common
 set of sophisticated compiler techniques to be applied at compile-time,
 link-time, install-time, run-time, or "idle-time" (between program runs).
 .
 This package contains examples for using LLVM, both in developing extensions
 to LLVM and in using it to compile code.

Package: llvm-runtime
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: binfmt-support
Description: Modular compiler and toolchain technologies (IR interpreter)
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 LLVM uses a single, language-independent virtual instruction set both as an
 offline code representation (to communicate code between compiler phases and
 to run-time systems) and as the compiler internal representation (to analyze
 and transform programs). This persistent code representation allows a common
 set of sophisticated compiler techniques to be applied at compile-time,
 link-time, install-time, run-time, or "idle-time" (between program runs).
 .
 This package provides the minimal required to execute programs in LLVM format.

Package: llvm-tools
Architecture: any
Depends: python3, python3-pygments, python3-yaml, ${misc:Depends}
Description: Modular compiler and toolchain technologies (tools)
 LLVM is a collection of libraries and tools that make it easy to build
 compilers, optimizers, just-in-time code generators, and many other
 compiler-related programs.
 .
 LLVM uses a single, language-independent virtual instruction set both as an
 offline code representation (to communicate code between compiler phases and
 to run-time systems) and as the compiler internal representation (to analyze
 and transform programs). This persistent code representation allows a common
 set of sophisticated compiler techniques to be applied at compile-time,
 link-time, install-time, run-time, or "idle-time" (between program runs).
 .
 This package provides tools for testing.

Package: python3-clang
Section: python
Architecture: any
Depends: libclang-dev, python3, ${misc:Depends}
Replaces: python-clang-x.y
Conflicts: python-clang-x.y
Provides: python-clang-x.y
Description: Clang Python Bindings
 Clang is a C, C++, CUDA and Objective C/C++ frontend based on the LLVM
 toolkit. It aims to offer a replacement to the GNU Compiler Collection (GCC).
 .
 Clang implements the ISO C++98/11/14/17/20/23 standards.
 .
 This binding package provides access to the Clang compiler and libraries.

Package: python3-lldb
Section: python
Architecture: any
Multi-Arch: same
Depends: liblldb (= ${binary:Version}), python3-six, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Conflicts: python3-lldb-x.y
Replaces: python3-lldb-x.y
Provides: python3-lldb-x.y
Description: Next generation, high-performance debugger, python3 lib
 LLDB is a next generation, high-performance debugger. It is built as a set of
 reusable components which highly leverage existing libraries in the larger
 LLVM project, such as the Clang expression parser and LLVM disassembler.
 .
 This binding package provides access to lldb.
